# -*- coding: utf-8 -*-
{
    'name': "Orignal customer_customize_NTN_GST_CNIC",

    'summary': """
        Added three fields in customer form""",

    'description': """
        Added three fields in customer form NTN, GST, CNIC
    """,

    'author': "Axiom World",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/flectra/flectra/blob/master/flectra/addons/base/module/module_data.xml
    # for the full list
    'category': 'Axiom World',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'stock'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/customer_customize.xml',

    ],
}