
from flectra import models, fields, api
from flectra.exceptions import ValidationError

class customer_customize__ntn__gst__cnic(models.Model):
    _inherit = 'res.partner'

    gst = fields.Float("GST")
    cnic = fields.Char("NTN/CNIC")

    @api.onchange('cnic')
    def _check_duplication(self):
        if self.cnic:
            temprary = self.env['res.partner'].search([])
            for prtnr_cnic in temprary:
                if prtnr_cnic.cnic == self.cnic:
                    raise ValidationError("CNIC/NTN is already exists: %s" % prtnr_cnic.cnic)