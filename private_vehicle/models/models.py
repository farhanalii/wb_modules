# -*- coding: utf-8 -*-

from flectra import models, fields, api
from flectra.exceptions import ValidationError

class PrivateVehicle(models.Model):
    _name= 'private.vehicle'

    name = fields.Char('Vehicle Number')
    vehicle_weight = fields.Float('Vehicle Weight', required=True)

    @api.onchange('name')
    def _check_duplication(self):
        if self.name:
            tmp = self.env['private.vehicle'].search([])
            for v_num in tmp:
                if v_num.name == self.name:
                    raise ValidationError("Vehicle Number is already exists: %s" % v_num.name)