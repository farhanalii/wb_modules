# -*- coding: utf-8 -*-
{
    'name': "Private Vehicle Detail",

    'summary': """
       In this moduel you can add Vehicle No & Vehicle Weight""",

    'description': """
       In this moduel you can add Vehicle No & Vehicle Weight
    """,

    'author': "Axiom World",
    'website': "http://www.axiomworld.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/flectra/flectra/blob/master/flectra/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
    ],
}