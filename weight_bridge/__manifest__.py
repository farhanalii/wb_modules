# -*- coding: utf-8 -*-
{
    'name': "Weight Bridge",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Axiom World",
    'website': "http://www.axiomworld.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/flectra/flectra/blob/master/flectra/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock', 'private_vehicle', 'sale_management','customer_customize__ntn__gst__cnic','sale_permit__gate_pass__invoice_titles_changes'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'data/ir_cron.xml'

    ],
}