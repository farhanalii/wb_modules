# -*- coding: utf-8 -*-

from flectra import models, api, fields
from flectra.exceptions import ValidationError
import xmlrpc.client as xc
import random
from typing import List
import serial
import io
import time
from serial.tools import list_ports

class weight_bridge(models.Model):
    _name = 'weight.bridge'

    state = fields.Selection([('draft', 'Draft'), ('validate', 'Validate')], default = 'draft')

    gatepass = fields.Char(required=True)
    customer = fields.Many2one('res.partner')
    product = fields.Many2one('product.product')
    vehicle = fields.Many2one('private.vehicle')
    first_weight = fields.Float('First Weight')
    second_weight = fields.Float()
    net_weight = fields.Float()
    driver_name = fields.Char("Driver Name")

    @api.onchange('vehicle')
    def get_first_weight(self):
        if self.vehicle:
            weight = self.env['private.vehicle'].search([('name', "=", self.vehicle.name)])
            self.first_weight = weight.vehicle_weight

    @api.onchange('second_weight')
    def get_net_weight(self):
        if self.second_weight:
            n_weight = self.second_weight - self.first_weight
            self.net_weight = n_weight

    @api.multi
    def validate(self):
        self.state = 'validate'
        print('sssssssssssssssssssssssssssssssss')

    @api.multi
    def get_data_from_port(self):

        ports = list(serial.tools.list_ports.comports())  # list of all Serial Ports store in ports
        def get_port():
            for port_no, description, address in ports:  # each port has three arguments PORT_NUMBER , DESCRIPTION, ADDRESS
                #  print(port_no , description , address)
                if 'USB' in address:  # The attached port has address USB other None
                    return port_no  # return our port_number

        port = get_port()
        if (port == "None"):
            raise ValidationError("Port Not Found")

        try:
            with serial.serial_for_url('spy://' + port + '?file=test.txt') as s:  # In serial_for_url spy is used to recive and transmitted
            #  In this while loop s.inWaiting() has acsii number passing through port
            #######
            # while True:
            #     for c in s.read():
            #         line.append(c)
            #         if c == '\n':
            #             print("Line: " + ''.join(line))
            #             line = []
            #             break

            ######## arduino device
                line = []
                count = 0
                joined_line = ''
                while True:
                    if count >= 1:
                        break

                    # print("1")
                    for c in s.read():
                        # print('2')
                        line.append(chr(c))  # convert from ANSII

                        joined_line = ''.join(str(v) for v in line)  # Make a string from array
                        # print(joined_line)

                        if chr(c) == '\n':
                            # print("Line " + str(count) + ': ' + joined_line)
                            line = []
                            self.second_weight = joined_line
                            count += 1
                            break

                        # while 1:
                        #     # print("sads")
                        #     if s.inWaiting() != 0:              #s.inWaiting() == 0 meant no data passed through port
                        #
                        #        print(s.in_waiting())
                        #        val = s.readline()   # when data pass it read the first line and store in val
                        #        s.write(val)                    # This line store dsta in test.txt

                        # s.dtr = False
                        # sprint(joined_line ).write(b'ABCD')
                        # byteData2 = s.in_waiting      # Get the number of bytes in the input buffer
                        # self.buffer.extend(self.s.read(1))
                        #
                        # self.buffer.extend(self.ser.read(self.ser.inWaiting()))
            s.close()

        except EnvironmentError:
            print(EnvironmentError)
            raise ValidationError("Connection is not establish")

            # with open('test.txt') as f:
            #     print(f.read())


    # @api.onchange('gatepass')
    def get_date_from_ghani_db(self):

        # Assigning variables
        url = 'http://148.251.153.13:8177'
        db = 'ghani'
        username = 'axiom'
        password = 'axiom123'

        # create connection
        common = xc.ServerProxy('{}/xmlrpc/2/common'.format(url))
        uid = common.authenticate(db, username, password, {})
        models = xc.ServerProxy('{}/xmlrpc/2/object'.format(url))
        # Getting data from Ghani_main_DB
        product_id = models.execute_kw(db, uid, password,
                                       'sale.order.line', 'search_read',
                                       [[['product_id', '!=', self.product.id]]],
                                       {'fields': ['product_id'],})

        for prod in product_id:
            product_obj = self.env['product.product'].search([('default_code','=',prod['product_id'][1].split('[')[1].split(']')[0])])
            if not product_obj:
                all = self.env['product.category'].search(
                    [('name', '=', 'All')])
                product_obj.create({
                    'name' : prod['product_id'][1].split('[')[1].split(']')[1],
                    'default_code' : prod['product_id'][1].split('[')[1].split(']')[0],
                    'type' : 'product',
                    'categ_id' : all.id,
                })


        data_from_ghani = models.execute_kw(db, uid, password,
                                            'stock.picking', 'search_read',
                                            [[['name', '!=', 'WH/OUT/'+str(self.gatepass)], ['state', '=', 'deliveryorder']]],
                                            {'fields': ['name','partner_id','vehicle_no','empty_weight', 'driver_name', 'driver_cnic', 'product_id', 'driver_name', 'customer_cnic'],})

        for ghani_data in data_from_ghani:
            res = False
            product_obj = self.env['product.product'].search(
                [('default_code', '=', ghani_data['product_id'][1].split('[')[1].split(']')[0])])
            customer = self.env['res.partner'].search([('cnic', '=', ghani_data['customer_cnic'])])
            if not customer:
                res = customer.create({
                    'name' : ghani_data['partner_id'][1],
                    'cnic': ghani_data['customer_cnic'],
                })
            else:
                res = customer

            veh = False
            vehicle = self.env['private.vehicle'].search([('name', '=', ghani_data['vehicle_no'][1])])
            if not vehicle:
                veh = vehicle.create({
                    'name': ghani_data['vehicle_no'][1],
                    'vehicle_weight': ghani_data['empty_weight'],
                })
            else:
                veh = vehicle

            weight_bridge_data = self.env['weight.bridge'].search([('gatepass','=', ghani_data['name'].split('/')[-1])])
            if not weight_bridge_data:
                weight_bridge_data.create({
                    'gatepass' : ghani_data['name'].split('/')[-1],
                    'customer' : res.id,
                    'vehicle' : veh.id,
                    'first_weight' : ghani_data['empty_weight'],
                    'driver_name' : ghani_data['driver_name'],
                    'product' : product_obj.id,
                })