# -*- coding: utf-8 -*-

from flectra import models, fields, api, _
from flectra.exceptions import UserError, ValidationError
from flectra.tools.float_utils import float_compare, float_is_zero, float_round
from flectra.exceptions import ValidationError


class ProductTempleteExt(models.Model):
    _inherit = ['product.template']

    # default_code = fields.Char("Internal Reference", required=True)

    @api.onchange('default_code')
    def _check_duplication(self):
        if self.default_code:
            temp = self.env['product.template'].search([])
            for d_code in temp:
                if d_code.default_code == self.default_code:
                    raise ValidationError("Internal Reference is already exists: %s" % d_code.default_code)

class delivery_challan_reciept_field(models.Model):
    _inherit = ['stock.picking']

    state = fields.Selection([('deliveryorder', 'Delivery Order'), ('draft', 'Draft'), ('waiting', 'Waiting Another Operation'), ('confirmed', 'Waiting'), ('assigned', 'Ready'), ('done', 'Done'), ('cancel', 'cancelled')])
    r1 = fields.Many2one('account.payment',string="Reciepts No")
    vehicle_no = fields.Many2one('private.vehicle', 'Vehicle No')
    driver_name = fields.Char("Driver Name")
    driver_cnic = fields.Char("Driver CNIC")
    check = fields.Boolean(default=False)
    empty_weight = fields.Float('Empty Weight')
    customer_cnic = fields.Char('cnic', related='partner_id.cnic')

    @api.multi
    def confirm_do(self):
        self.state = 'deliveryorder'

    @api.onchange('vehicle_no')
    def _get_empty_weight(self):
        if self.vehicle_no:
            emp_weight = self.env['private.vehicle'].search([('name', "=", self.vehicle_no.name)])
            self.empty_weight = emp_weight.vehicle_weight

    @api.onchange('partner_id')
    def onchange_partner(self):
        return {'domain': {'r1': [('partner_id', '=', self.partner_id.id or False)]}, }

    @api.multi
    def button_validate(self):
        self.ensure_one()
        if not self.move_lines and not self.move_line_ids:
            raise ValidationError(_('Please add some lines to move'))
        if not self.r1:
            if self.check is False:
                raise ValidationError(_('Please Add Reciept First!'))

        # If no lots when needed, raise error
        picking_type = self.picking_type_id
        no_quantities_done = all(
            float_is_zero(move_line.qty_done, precision_rounding=move_line.product_uom_id.rounding) for move_line in
            self.move_line_ids)
        no_reserved_quantities = all(
            float_is_zero(move_line.product_qty, precision_rounding=move_line.product_uom_id.rounding) for move_line in
            self.move_line_ids)
        if no_reserved_quantities and no_quantities_done:
            raise ValidationError(_(
                'You cannot validate a transfer if you have not processed any quantity. You should rather cancel the transfer.'))

        if picking_type.use_create_lots or picking_type.use_existing_lots:
            lines_to_check = self.move_line_ids
            if not no_quantities_done:
                lines_to_check = lines_to_check.filtered(
                    lambda line: float_compare(line.qty_done, 0,
                                               precision_rounding=line.product_uom_id.rounding)
                )

            for line in lines_to_check:
                product = line.product_id
                if product and product.tracking != 'none':
                    if not line.lot_name and not line.lot_id:
                        raise ValidationError(_('You need to supply a lot/serial number for %s.') % product.display_name)
                    elif line.qty_done == 0:
                        raise ValidationError(_(
                            'You cannot validate a transfer if you have not processed any quantity for %s.') % product.display_name)

        if no_quantities_done:
            view = self.env.ref('stock.view_immediate_transfer')
            wiz = self.env['stock.immediate.transfer'].create({'pick_ids': [(4, self.id)]})
            return {
                'name': _('Immediate Transfer?'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'stock.immediate.transfer',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': wiz.id,
                'context': self.env.context,
            }

        if self._get_overprocessed_stock_moves() and not self._context.get('skip_overprocessed_check'):
            view = self.env.ref('stock.view_overprocessed_transfer')
            wiz = self.env['stock.overprocessed.transfer'].create({'picking_id': self.id})
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'stock.overprocessed.transfer',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': wiz.id,
                'context': self.env.context,
            }

        # Check backorder should check for other barcodes
        if self._check_backorder():
            return self.action_generate_backorder_wizard()
        self.action_done()
        return
